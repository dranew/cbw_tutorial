## Environment setup

For this tutorial, we now assume the environment variable `$TUTORIAL_HOME`
has been set to an existing directory to which the user has write access.

All binaries used in this tutorial will be installed using [`conda`](https://www.continuum.io/downloads).  Modify the `PATH` environment variable to point to binaries in the anaconda installation.

```
export PATH="/home/ubuntu/CourseData/CG_data/Module4/anaconda/bin:$PATH"
```

