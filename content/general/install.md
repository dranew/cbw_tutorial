# General installation for CBW tutorial

<#include "env.md"> 

## Installation

### Tutorial directory structure

Create a directory for the reference data.

```{.bash}
mkdir -p $TUTORIAL_HOME/refdata
```

### Install tutorial scripts

All content for this tutorial is located in a bitbucket repo at
`https://dranew@bitbucket.org/dranew/cbw_tutorial.git`.  Some of the data, scripts and config files from this repo will be used in the tutorial.  Clone the tutorial repo so we have a copy of the tutorial scripts in a known location.

```{.bash}
cd $TUTORIAL_HOME/
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/general/clone_tutorial}
git clone https://bitbucket.org/dranew/cbw_tutorial.git
```

### Install Anaconda

All binaries used in this tutorial will be installed using [`conda`](https://www.continuum.io/downloads).  Download and install anaconda with the prefix `$TUTORIAL_HOME/anaconda`.

Packages in `conda` are stored in channels.  Several additional channels hosting bioinformatics specific software will be required.  Add additional channels using `conda config`.

```
conda config --add channels r
conda config --add channels bioconda
conda config --add channels BioBuilds
conda config --add channels https://conda.anaconda.org/dranew
```

### Install samtools

The `samtools` package is the most widely used software for manipulating high-throughput sequence data stored in the 'bam' format.

Install `samtools` using `conda`.

```{.bash}
conda install samtools
```

### Install picard tools

Picard tools is a useful set of utilities for manipulating sequence data in bam/sam format.

Install `picard` using `conda`.

```{.bash}
conda install picard
```

### Install igv tools

The igvtools package provides utilities for preprocessing bam files for quicker viewing in IGV.

Install `igvtools` using `conda`.

```{.bash}
conda install igvtools
```

### Install bowtie and bowtie2

Install `bowtie` and `bowtie2` using `conda`.

```{.bash}
conda install bowtie bowtie2
```

### Install the gmap aligner

Install `gmap` using `conda`.

```{.bash}
conda install gmap
```

### Install the bwa aligner

Install `bwa` using `conda`.

```{.bash}
conda install bwa
```

