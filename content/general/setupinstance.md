## Create tutorial directory structure on the instance

Create a directory in the temporary workspace and change to that directory.

```{.bash}
mkdir /mnt/workspace/Module4/
cd /mnt/workspace/Module4/
```

All content for this tutorial is located in a bitbucket repo at
`https://dranew@bitbucket.org/dranew/cbw_tutorial.git`.  Some of the data, scripts and config files from this repo will be used in the tutorial.  Link the repo directory to your workspace.

```{.bash}
ln -s /media/cbwdata/CG_data/Module4/cbw_tutorial
```

The reference genomes have been indexed and fusion caller specific setup scripts have been run for you.  Instructions for your reference are available in the install section of the tutorial.  Link the ref data to your workspace.

```{.bash}
ln -s /media/cbwdata/CG_data/Module4/refdata
```

Sample data has been prepared for you, link to your workspace.

```{.bash}
ln -s /media/cbwdata/CG_data/Module4/sampledata
```

For visualization purposes, a list of bams is available for reads aligning to fused genes.  Link those into your workspace.

```{.bash}
ln -s /media/cbwdata/CG_data/Module4/bams
```

Set the `$TUTORIAL_HOME` environment variable so subsequent commands know the 
locatioin of installed binaries, libraries, and reference genome data.  Also,
subsequent analyses will fall into subdirectories of `$TUTORIAL_HOME`.

```{.bash}
TUTORIAL_HOME=/mnt/workspace/Module4
```
