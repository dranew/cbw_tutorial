gpp -U "<#" ">" "\B" "|" ">" "<" ">" "#" "" run.md \
	| pandoc -t mediawiki \
	| sed 's#&quot;#"#' \
	| sed 's#&gt;#>#' \
	| sed 's#&lt;#<#' \
	> site/run.wiki

gpp -U "<#" ">" "\B" "|" ">" "<" ">" "#" "" install.md \
	| pandoc -t mediawiki \
	| sed 's#&quot;#"#' \
	| sed 's#&gt;#>#' \
	| sed 's#&lt;#<#' \
	> site/install.wiki

cat exploration.md | pandoc -t mediawiki -o site/exploration.wiki
