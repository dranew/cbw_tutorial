import sys

sys.stdout.write('''---
layout: post3
title: Bioinformatics for Cancer Genomics 2016 Gene Fusions Tutorial
header1: Bioinformatics for Cancer Genomics 2016
header2: Gene Fusions Tutorial
image: fusions.png
---
''')

first_line = True
skipping = False
for line in sys.stdin:
    if first_line and line.startswith('---'):
        skipping = True
    elif skipping and line.startswith('---'):
        skipping = False
        continue
    first_line = False
    if not skipping:
        sys.stdout.write(line)
