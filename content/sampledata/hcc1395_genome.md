# Downloading and preparing HCC1395 WGS sample data

## Environment setup

We will create environment variables pointing to the data we download, and set a
sample id for the data.  We will be working with a publically available HCC1395
breast cancer cell line sequenced for teaching and benchmarking purposes.  The
dataset is available from washington university servers.

```{.bash}
SAMPLE_ID=HCC1395_genome
SAMPLE_NORMAL_LANE_1_RAW_BAM=gerald_D1VCPACXX_6.bam
SAMPLE_NORMAL_LANE_2_RAW_BAM=gerald_D1VCPACXX_7.bam
SAMPLE_NORMAL_LANE_3_RAW_BAM=gerald_D1VCPACXX_8.bam
SAMPLE_TUMOUR_LANE_1_RAW_BAM=gerald_D1VCPACXX_1.bam
SAMPLE_TUMOUR_LANE_2_RAW_BAM=gerald_D1VCPACXX_2.bam
SAMPLE_TUMOUR_LANE_3_RAW_BAM=gerald_D1VCPACXX_3.bam
SAMPLE_TUMOUR_LANE_4_RAW_BAM=gerald_D1VCPACXX_4.bam
SAMPLE_TUMOUR_LANE_5_RAW_BAM=gerald_D1VCPACXX_5.bam
SAMPLE_NORMAL_BAM=$TUTORIAL_HOME/sampledata/$SAMPLE_ID/normal.bam
SAMPLE_TUMOUR_BAM=$TUTORIAL_HOME/sampledata/$SAMPLE_ID/tumour.bam
SAMPLE_URL_PREFIX=https://xfer.genome.wustl.edu/gxfer1/project/gms/testdata/bams/hcc1395/
```

## Download lanes

We will be working in a sample specific directory.  Create a directory for temporary
files.

```{.bash}
mkdir -p $TUTORIAL_HOME/sampledata/$SAMPLE_ID
mkdir -p $TUTORIAL_HOME/sampledata/$SAMPLE_ID/temp/

cd $TUTORIAL_HOME/sampledata/$SAMPLE_ID/temp/
```

Use wget to download the sample data.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/wget}
wget $SAMPLE_URL_PREFIX/$SAMPLE_NORMAL_LANE_1_RAW_BAM
wget $SAMPLE_URL_PREFIX/$SAMPLE_NORMAL_LANE_2_RAW_BAM
wget $SAMPLE_URL_PREFIX/$SAMPLE_NORMAL_LANE_3_RAW_BAM
wget $SAMPLE_URL_PREFIX/$SAMPLE_TUMOUR_LANE_1_RAW_BAM
wget $SAMPLE_URL_PREFIX/$SAMPLE_TUMOUR_LANE_2_RAW_BAM
wget $SAMPLE_URL_PREFIX/$SAMPLE_TUMOUR_LANE_3_RAW_BAM
wget $SAMPLE_URL_PREFIX/$SAMPLE_TUMOUR_LANE_4_RAW_BAM
wget $SAMPLE_URL_PREFIX/$SAMPLE_TUMOUR_LANE_5_RAW_BAM
```

## Align bam files using bwa mem

The `bwa mem` aligner takes fastq as input but input can be piped from `samtools fastq`.
We can concatenate multiple lanes using `samtools cat` into `samtools fastq`.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/bwa_mem}
samtools cat \
    $SAMPLE_NORMAL_LANE_1_RAW_BAM \
    $SAMPLE_NORMAL_LANE_2_RAW_BAM \
    $SAMPLE_NORMAL_LANE_3_RAW_BAM |
    samtools fastq |
    bwa mem -a -p -t 48 $ENSEMBL_GENOME_FILENAME - >
    > normal.sam
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/bwa_mem}
samtools cat -o tumour_raw.bam \
    $SAMPLE_TUMOUR_LANE_1_RAW_BAM \
    $SAMPLE_TUMOUR_LANE_2_RAW_BAM \
    $SAMPLE_TUMOUR_LANE_3_RAW_BAM \
    $SAMPLE_TUMOUR_LANE_4_RAW_BAM \
    $SAMPLE_TUMOUR_LANE_5_RAW_BAM |
    samtools fastq |
    bwa mem -a -p -t 48 $ENSEMBL_GENOME_FILENAME - >
    > tumour.sam
```

Convert sam to bam using `samtools view`, sort using `samtools sort` and create an index
using `samtools index`.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/samtools_view}
samtools view -bt $ENSEMBL_GENOME_FILENAME \
    normal.sam \
    > normal.bam
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/samtools_sort}
samtools sort -m 20G \
    -o normal.bam \
    normal.sort \
    > $SAMPLE_NORMAL_BAM
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/samtools_index}
samtools index $SAMPLE_NORMAL_BAM
```

Use a similar set of commands to create the tumour bam file.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/bwa_aln1}
bwa aln $ENSEMBL_GENOME_FILENAME \
    -b1 tumour_raw.bam \
    > tumour_1.sai
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/bwa_aln2}
bwa aln $ENSEMBL_GENOME_FILENAME \
    -b2 tumour_raw.bam \
    > tumour_2.sai
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/bwa_sampe}
bwa sampe $ENSEMBL_GENOME_FILENAME \
    tumour_1.sai \
    tumour_2.sai \
    tumour_raw.bam \
    tumour_raw.bam \
    > tumour.sam
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/samtools_view}
samtools view -bt $ENSEMBL_GENOME_FILENAME \
    tumour.sam \
    > tumour.bam
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/samtools_sort}
samtools sort -m 20G \
    -o tumour.bam \
    tumour.sort \
    > $SAMPLE_TUMOUR_BAM
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/samtools_index}
samtools index $SAMPLE_TUMOUR_BAM
```

