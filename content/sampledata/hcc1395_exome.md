# Downloading and preparing HCC1395 exome sample data

## Environment setup

We will create environment variables pointing to the data we download, and set a
sample id for the data.  We will be working with a publically available HCC1395
breast cancer cell line sequenced for teaching and benchmarking purposes.  The
dataset is available from washington university servers.

```{.bash}
SAMPLE_ID=HCC1395_exome
SAMPLE_NORMAL_RAW_BAM=gerald_C1TD1ACXX_7_CGATGT.bam
SAMPLE_TUMOUR_RAW_BAM=gerald_C1TD1ACXX_7_ATCACG.bam
SAMPLE_NORMAL_BAM=$TUTORIAL_HOME/sampledata/$SAMPLE_ID/normal.bam
SAMPLE_TUMOUR_BAM=$TUTORIAL_HOME/sampledata/$SAMPLE_ID/tumour.bam
SAMPLE_URL_PREFIX=https://xfer.genome.wustl.edu/gxfer1/project/gms/testdata/bams/hcc1395/
```

## Download

We will be working in a sample specific directory.  Create a directory for temporary
files.

```{.bash}
mkdir -p $TUTORIAL_HOME/sampledata/$SAMPLE_ID
mkdir -p $TUTORIAL_HOME/sampledata/$SAMPLE_ID/temp/

cd $TUTORIAL_HOME/sampledata/$SAMPLE_ID/temp/
```

Use wget to download the sample data.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/wget}
wget $SAMPLE_URL_PREFIX/$SAMPLE_NORMAL_RAW_BAM
wget $SAMPLE_URL_PREFIX/$SAMPLE_TUMOUR_RAW_BAM
```

## Align bam files using bwa

The `bwa` aligner can take bam as input.  Alignment is in 3 steps, 1 step to align each
read end (`bwa aln`), and a subsequent step to create a sam file of paired end
alignments (`bwa sampe`).

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/bwa_aln1}
bwa aln $ENSEMBL_GENOME_FILENAME \
    -b1 $SAMPLE_NORMAL_RAW_BAM \
    > normal_1.sai
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/bwa_aln2}
bwa aln $ENSEMBL_GENOME_FILENAME \
    -b2 $SAMPLE_NORMAL_RAW_BAM \
    > normal_2.sai
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/bwa_sampe}
bwa sampe $ENSEMBL_GENOME_FILENAME \
    normal_1.sai \
    normal_2.sai \
    $SAMPLE_NORMAL_RAW_BAM \
    $SAMPLE_NORMAL_RAW_BAM \
    > normal.sam
```

Convert sam to bam using `samtools view`, sort using `samtools sort` and create an index
using `samtools index`.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/samtools_view}
samtools view -bt $ENSEMBL_GENOME_FILENAME \
    normal.sam \
    > normal.bam
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/samtools_sort}
samtools sort -o normal.bam \
    normal.sort \
    > $SAMPLE_NORMAL_BAM
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/normal/samtools_index}
samtools index $SAMPLE_NORMAL_BAM
```

Use a similar set of commands to create the tumour bam file.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/bwa_aln1}
bwa aln $ENSEMBL_GENOME_FILENAME \
    -b1 $SAMPLE_TUMOUR_RAW_BAM \
    > tumour_1.sai
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/bwa_aln2}
bwa aln $ENSEMBL_GENOME_FILENAME \
    -b2 $SAMPLE_TUMOUR_RAW_BAM \
    > tumour_2.sai
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/bwa_sampe}
bwa sampe $ENSEMBL_GENOME_FILENAME \
    tumour_1.sai \
    tumour_2.sai \
    $SAMPLE_TUMOUR_RAW_BAM \
    $SAMPLE_TUMOUR_RAW_BAM \
    > tumour.sam
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/samtools_view}
samtools view -bt $ENSEMBL_GENOME_FILENAME \
    tumour.sam \
    > tumour.bam
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/samtools_sort}
samtools sort -o tumour.bam \
    tumour.sort \
    > $SAMPLE_TUMOUR_BAM
```

```{.bash sentinal=$TUTORIAL_HOME/sentinal/sampledata/$SAMPLE_ID/tumour/samtools_index}
samtools index $SAMPLE_TUMOUR_BAM
```

