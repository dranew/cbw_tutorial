# HCC1395 sample data

## Environment setup

We will create environment variables pointing to the data on the instance, and
set a sample id for the data.

We will be working with a publically available HCC1395 breast cancer cell line
sequenced for teaching and benchmarking purposes.  The dataset is available 
from washington university servers, and can be accessed via the github page for the
[Genome Modeling System](https://github.com/genome/gms/wiki/HCC1395-WGS-Exome-RNA-Seq-Data).

Special thanks to Malachi Griffith for providing access to the data.

```{.bash}
SAMPLE_ID=HCC1395
SAMPLE_FASTQ_1=/media/cbwdata/CG_data/HCC1395/rnaseq/SAMPLE_R1.fastq
SAMPLE_FASTQ_2=/media/cbwdata/CG_data/HCC1395/rnaseq/SAMPLE_R2.fastq
```

