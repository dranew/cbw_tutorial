
# General Setup

<#include "general/setupinstance.md">

<#include "general/env.md">

<#include "genome/singlechrom/env.md">

<#include "sampledata/hcc1395_instance.md">

<#include "times.md">

<#include "chimerascan/run.md">

<#include "defuse/run.md">

<#include "star/run.md">

<#include "tophat_fusion/run.md">

<#include "trinity/run.md">
