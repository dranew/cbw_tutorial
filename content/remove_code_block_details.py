import sys

for line in sys.stdin:
    if line.startswith('```'):
        line = '~~~' + line[3:]
    if line.startswith('~~~{.bash'):
        line = '~~~ bash\n'
    sys.stdout.write(line)
