gpp -U "<#" ">" "\B" "|" ">" "<" ">" "#" "" run.md \
	| python remove_code_block_details.py \
	| python add_yaml_header.py \
	> site/run.md

gpp -U "<#" ">" "\B" "|" ">" "<" ">" "#" "" install.md \
	| python remove_code_block_details.py \
	| python add_yaml_header.py \
	> site/install.md

cat exploration.md \
	| python remove_code_block_details.py \
	| python add_yaml_header.py \
	> site/exploration.md

cat plot/genefusions.md \
	| python add_yaml_header.py \
	> site/visualization.md

cp plot/*.png site/
