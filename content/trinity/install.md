# Installation of the Trinity RNA-Seq assembler

<#include "env.md">

## Installation

Install `trinity` using `conda`

```{.bash}
conda install trinity
```

