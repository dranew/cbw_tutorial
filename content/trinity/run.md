# Run the Trinity RNA-Seq assembler / GMAP contig mapper

<#include "env.md">

## Execution

### Assemble using Trinity

Create a directory for the trinity analysis.  Run trinity with fastq (fq) as the sequence type.  Specify memory and threads, and provide the fastq paths and output paths.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/trinity/assemble}
mkdir -p $TUTORIAL_HOME/analysis/trinity/$SAMPLE_ID/trinity/

cp $SAMPLE_FASTQ_1 $TUTORIAL_HOME/analysis/trinity/$SAMPLE_ID/sample_reads_1.fq
cp $SAMPLE_FASTQ_2 $TUTORIAL_HOME/analysis/trinity/$SAMPLE_ID/sample_reads_2.fq

Trinity \
    --seqType fq --max_memory 12G --CPU 4 \
    --left $TUTORIAL_HOME/analysis/trinity/$SAMPLE_ID/sample_reads_1.fq \
    --right $TUTORIAL_HOME/analysis/trinity/$SAMPLE_ID/sample_reads_2.fq \
    --output $TUTORIAL_HOME/analysis/trinity/$SAMPLE_ID/trinity/
```

### Align using GMap

Use gmap to map the resulting contigs to the reference genome and produce a GFF3
file.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/trinity/gmap}
cd $TUTORIAL_HOME/analysis/trinity/$SAMPLE_ID/trinity

gmap \
    -f gff3_gene \
    -D $GMAP_INDEX_DIR \
    -d $GMAP_REF_ID \
    Trinity.fasta \
    > Trinity.gff3
```

### Postprocess

Post-process the gff3 file to produce a list of fusions.  We will use a custom
script to simply pull out chimeric contigs.  Realistically, further processing
would be required to identify true fusions.

```{.bash}
cd $TUTORIAL_HOME/analysis/trinity/$SAMPLE_ID/trinity

python $TUTORIAL_HOME/cbw_tutorial/scripts/gmap_extract_fusions.py \
    Trinity.gff3 Trinity.fasta Fusions.fasta
```

The following sequence is the `PLA2R-RBMS1` fusion.

```
>TRINITY_DN532_c0_g1_i1 len=712 path=[1379:0-711] [-1, 1379, -2]
TTTTGTGAATGAGCTCTTACATTCAAAATTTAATTGGACAGAAGAAAGGCAGTTCTGGAT
TGGATTTAATAAAAGAAACCCACTGAATGCCGGCTCATGGGAGTGGTCTGATAGAACTCC
TGTTGTCTCTTCGTTTTTAGACAACACTTATTTTGGAGAAGATGCAAGAAACTGTGCTGT
TTATAAGGCAAACAAAACATTGCTGCCCTTACACTGTGGTTCCAAACGTGAATGGATATG
CAAAATCCCAAGAGATGTGAAACCCAAGATTCCGTTCTGGTACCAGTACGATGTACCCTG
GCTCTTTTATCAGGATGCAGAATACCTTTTTCATACCTTTGCCTCAGAATGGTTGAACTT
TGAGTTTGTCTGTAGCTGGCTGCACAGTGATCTTCTCACAATTCATTCTGCACATGAGCA
AGAATTCATCCACAGCAAAATAAAAGCGCAACAGGAACAAGATCCTACCAACCTCTACAT
TTCTAATTTGCCACTCTCCATGGATGAGCAAGAACTAGAAAATATGCTCAAACCATTTGG
ACAAGTTATTTCTACAAGGATACTACGTGATTCCAGTGGTACAAGTCGTGGTGTTGGCTT
TGCTAGGATGGAATCAACAGAAAAATGTGAAGCTGTTATTGGTCATTTTAATGGAAAATT
TATTAAGACACCACCAGGAGTTTCTGCCCCCACAGAACCTTTATTGTGTAAG
```



