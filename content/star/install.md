# Installation of the STAR RNA-Seq aligner

<#include "env.md"> 

## Installation

Install STAR using conda.  Also install perl and the perl package Set::IntervalTree, required by STAR-Fusion.

```
conda install perl-threaded
conda install perl-set-intervaltree
conda install star
```

## Create genome 

For STAR-Fusion, we require an additional reference dataset.

```
cd $STAR_GENOME_INDEX

wget https://data.broadinstitute.org/Trinity/CTAT_RESOURCE_LIB/GRCh37_gencode_v19_CTAT_lib.tar.gz

tar -xvf GRCh37_gencode_v19_CTAT_lib.tar.gz

cd GRCh37_gencode_v19_CTAT_lib/
```

Optionally subset the reference for the tutorial chromosomes.

```
samtools faidx ref_genome.fa chr$TUTORIAL_CHROMOSOME \
    > ref_genome.chr$TUTORIAL_CHROMOSOME.fa
mv ref_genome.chr$TUTORIAL_CHROMOSOME.fa ref_genome.fa
rm ref_genome.fa.fai

grep -P "^chr$TUTORIAL_CHROMOSOME\t" ref_annot.gtf \
    > ref_annot.chr$TUTORIAL_CHROMOSOME.gtf
mv ref_annot.chr$TUTORIAL_CHROMOSOME.gtf ref_annot.gtf
```

Prepare the genome and annotations for star fusion.

```
prep_genome_lib.pl \
    --genome_fa ref_genome.fa \
    --gtf ref_annot.gtf \
    --blast_pairs blast_pairs.outfmt6.gz
```


