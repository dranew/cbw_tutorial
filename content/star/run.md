# Run the STAR RNA-Seq aligner on a sample

<#include "env.md">

## Execution

### Run the star aligner

Running STAR on paired end read fastqs can be performed in a single step.  Set the prefix argument `--outFileNamePrefix` to specify the location of the output.

Assume we have end 1 and end 2 files as `$SAMPLE_FASTQ_1` and `$SAMPLE_FASTQ_2` environment variables pointing to paired fastq files for sample `$SAMPLE_ID`.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/star/align}
mkdir -p $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/
STAR --runThreadN 4 \
    --outSAMtype BAM SortedByCoordinate \
    --outSAMstrandField intronMotif \
    --outFilterIntronMotifs RemoveNoncanonicalUnannotated \
    --outReadsUnmapped None \
    --chimSegmentMin 15 \
    --chimJunctionOverhangMin 15 \
    --chimOutType WithinBAM \
    --alignMatesGapMax 200000 \
    --alignIntronMax 200000 \
    --genomeDir $STAR_GENOME_INDEX \
    --readFilesIn $SAMPLE_FASTQ_1 $SAMPLE_FASTQ_2 \
    --outFileNamePrefix $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/
```

> Note: In order to obtain alignments of chimeric reads potentially supporting fusions, we
> have added the `--chimSegmentMin 20` option to obtain chimerica reads anchored by at least
> 20nt on either side of the fusion boundary, and `--chimOutTypeWithinBAM` to report such
> alignments in the sam/bam output.

> Note: We are running with additional command line options for fusion discovery as described
> in the manual for STAR-Fusion.

The file `$TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Aligned.sortedByCoord.out.bam` should contain
the resulting alignments in bam format, sorted by position.

Index the bam file using `samtools index`.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/star/bamindex}
samtools index $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Aligned.sortedByCoord.out.bam
```

Generate a precalculated coverage file for use with IGV.  This will 
allow viewing of read depth across the genome at all scales, and will
speed up IGV viewing of the bam file.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/star/genomecov}
igvtools count $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Aligned.sortedByCoord.out.bam \
    $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Aligned.sortedByCoord.out.bam.tdf hg19
```

You can view the bam file in IGV at:

```
http://cbw#.dyndns.info/Module4/analysis/star/HCC1395/Aligned.sortedByCoord.out.bam
```

where you need to replace # with your student ID.

### Run STAR fusion caller

The STAR fusion caller parses the chimeric alignments produced by the STAR aligner
and predicts fusions from these alignments.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/star/fusions}
STAR-Fusion \
    --chimeric_junction $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Chimeric.out.junction \
    --genome_lib_dir $STAR_FUSION_GENOME_INDEX \
    --output_dir $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/starfusion
```

The fusion reads are output to the sam file `Chimeric.out.sam`.  To view these in IGV, first import into bam format, sort, and then index.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/star/chimericbam}
samtools view -bt $UCSC_GENOME_FILENAME \
    $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Chimeric.out.sam \
    | samtools sort - -o $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Chimeric.out.bam
samtools index $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Chimeric.out.bam
igvtools count $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Chimeric.out.bam \
    $TUTORIAL_HOME/analysis/star/$SAMPLE_ID/Chimeric.out.bam.tdf hg19
```

You can view the bam file of fusion reads in IGV at:

```
http://cbw#.dyndns.info/Module4/analysis/star/HCC1395/Chimeric.out.bam
```

where you need to replace # with your student ID.  Fusion reads can be found
at this location `chr2:160,832,184-160,837,535`.


