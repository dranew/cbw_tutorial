# Run the ChimeraScan fusion prediction tool

<#include "env.md">

## Execution

Run chimerascan by providing the index directory, the pair of fastq files, and
the sample specific output directory

```{.bash sentinal=$TUTORIAL_HOME/sentinal/chimerascan/run}
mkdir -p $TUTORIAL_HOME/analysis/chimerascan/$SAMPLE_ID/
chimerascan_run.py -p 4 \
    $CHIMERASCAN_INDEX \
    $SAMPLE_FASTQ_1 $SAMPLE_FASTQ_2 \
    $TUTORIAL_HOME/analysis/chimerascan/$SAMPLE_ID/
```

After chimerascan has completed we can run an additional script to create an
html output to browse the results.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/chimerascan/createhtml}
chimerascan_html_table.py --read-throughs \
    -o $TUTORIAL_HOME/analysis/chimerascan/$SAMPLE_ID/chimeras.html \
    $TUTORIAL_HOME/analysis/chimerascan/$SAMPLE_ID/chimeras.bedpe
```

You can view the resulting html report at the following url in your browser, where you need to replace `#` with your instance id.

```
http://cbw#.dyndns.info/Module4/analysis/chimerascan/HCC1395/chimeras.html
```

