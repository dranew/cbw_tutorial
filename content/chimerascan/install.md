# Installation of the ChimeraScan gene fusion prediction tool

<#include "env.md">

## Installation

### Install ChimeraScan

Install in ChimeraScan using `conda`.

```{.bash}
conda install chimerascan
```

### Install the required reference data files

Install the reference data in a subdirectory of the tutorial ref data.

```{.bash}
mkdir -p $TUTORIAL_HOME/refdata/chimerascan/
cd $TUTORIAL_HOME/refdata/chimerascan/
```

Download the gene models from chimerascan's google code site as specified in the instructions.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/chimerascan/downloadgenome}
wget https://chimerascan.googlecode.com/files/hg19.ucsc_genes.txt.gz
gunzip hg19.ucsc_genes.txt.gz
```

Build the chimerascan indices using the `chimerascan_index.py` command.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/chimerascan/buildindices}
mkdir -p $CHIMERASCAN_INDEX
chimerascan_index.py \
    $UCSC_GENOME_FILENAME hg19.ucsc_genes.txt \
    $CHIMERASCAN_INDEX
```

