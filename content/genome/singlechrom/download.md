# Single chromosome reference

Instructions for downloading a _single chromosome_ reference genome and gene models.  We will use a single chromosome reference in the tutorial to save time and disk space.

> Note: For running the analyses in a real life setting refer to the
> instructions provided for using a full genome reference.

<#include "env.md">

## Download data

The following downloads a single chromosome and sets it as the reference genome for quicker execution on a smaller dataset.

Data will be stored in the `refdata` subdirectory.

```{.bash}
mkdir -p $TUTORIAL_HOME/refdata
cd $TUTORIAL_HOME/refdata
```

Download the ensembl gene models in GTF format.  Subset for the specified chromosome.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/singlechrom/wget_gtf}
wget ftp://ftp.ensembl.org/pub/release-75/gtf/homo_sapiens/Homo_sapiens.GRCh37.75.gtf.gz
gunzip -c Homo_sapiens.GRCh37.75.gtf.gz \
    | grep -P "(^$TUTORIAL_CHROMOSOME\t|^#\!)" \
    > $ENSEMBL_GTF_FILENAME
```

Download the gencode gene models in GTF format.  These are the prefered gene models for use with the STAR aligner.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/singlechrom/wget_gencode_gtf}
wget ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_19/gencode.v19.annotation.gtf.gz
gunzip -c gencode.v19.annotation.gtf.gz \
    | grep -P "(^chr$TUTORIAL_CHROMOSOME\t|^##)" \
    > $GENCODE_GTF_FILENAME
```

Download the chromosome from ensembl.  Also run `samtools faidx` to create an index of the fasta as this is required by many samtools related commands.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/singlechrom/wget_chromosome}
wget ftp://ftp.ensembl.org/pub/release-75/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.75.dna.chromosome.$TUTORIAL_CHROMOSOME.fa.gz
gunzip Homo_sapiens.GRCh37.75.dna.chromosome.$TUTORIAL_CHROMOSOME.fa.gz

samtools faidx Homo_sapiens.GRCh37.75.dna.chromosome.$TUTORIAL_CHROMOSOME.fa
```

Download the chromosome from ucsc, used by some tools.  Also run `samtools faidx`.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/singlechrom/wget_ucsc_chr14}
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/chromosomes/chr$TUTORIAL_CHROMOSOME.fa.gz .
gunzip chr$TUTORIAL_CHROMOSOME.fa.gz

samtools faidx chr$TUTORIAL_CHROMOSOME.fa
```

### Index the reference genome for specific aligners

Build bowtie indices of the ensembl genome and ucsc genome.  Here the second argument is the prefix of the fasta files, which will be how we refer to these indices in the future.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/singlechrom/bowtie_build}
bowtie-build $ENSEMBL_GENOME_FILENAME $ENSEMBL_GENOME_PREFIX
bowtie2-build $ENSEMBL_GENOME_FILENAME $ENSEMBL_GENOME_PREFIX
bowtie-build $UCSC_GENOME_FILENAME $UCSC_GENOME_PREFIX
bowtie2-build $UCSC_GENOME_FILENAME $UCSC_GENOME_PREFIX
```

Build the gmap index for the ensembl genome.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/singlechrom/gmap_build}
mkdir $GMAP_INDEX_DIR
gmap_build -D $GMAP_INDEX_DIR -d $GMAP_REF_ID $ENSEMBL_GENOME_FILENAME
```

Build the bwa index for the ensembl genome.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/singlechrom/bwa_build_ens_genome}
bwa index $ENSEMBL_GENOME_FILENAME
```

Index the genome for STAR.  Here we need to include the reference genome fasta and the gene models in GTF format.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/star/indexgenome}
mkdir $STAR_GENOME_INDEX
STAR --runThreadN 1 \
    --runMode genomeGenerate \
    --genomeDir $STAR_GENOME_INDEX \
    --genomeFastaFiles $UCSC_GENOME_FILENAME \
    --sjdbGTFfile $GENCODE_GTF_FILENAME \
    --sjdbOverhang 100
```



