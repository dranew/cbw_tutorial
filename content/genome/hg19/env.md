## Environment setup

Create variables for the reference genome and gene models.

```{.bash}
ENSEMBL_GTF_FILENAME=$TUTORIAL_HOME/refdata/Homo_sapiens.GRCh37.75.gtf
GENCODE_GTF_FILENAME=$TUTORIAL_HOME/refdata/gencode.v19.annotation.gtf
ENSEMBL_GENOME_FILENAME=$TUTORIAL_HOME/refdata/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa
UCSC_GENOME_FILENAME=$TUTORIAL_HOME/refdata/hg19.fa
ENSEMBL_GENOME_PREFIX=$TUTORIAL_HOME/refdata/Homo_sapiens.GRCh37.75.dna.primary_assembly
UCSC_GENOME_PREFIX=$TUTORIAL_HOME/refdata/hg19
```

For gmap, set a directory for the gmap indices, and the id of the reference.

```{.bash}
GMAP_REF_ID=hg19
GMAP_INDEX_DIR=$TUTORIAL_HOME/refdata/gmap/
```

