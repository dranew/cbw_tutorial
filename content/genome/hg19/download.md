# Full genome reference

Instructions for downloading the genome and gene model reference data and indexing the
fasta files for use with short read aligners.  Note that in the tutorial we will be using
a single chromosome as a reference to save time and disk space.

<#include "env.md">

## Download data

The following downloads the full human genome and sets it as the reference genome.

Data will be stored in the `refdata` subdirectory.

```{.bash}
cd $TUTORIAL_HOME/refdata
```

Download the ensembl gene models in GTF format.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/hg19/wget_gtf}
wget ftp://ftp.ensembl.org/pub/release-75/gtf/homo_sapiens/Homo_sapiens.GRCh37.75.gtf.gz
gunzip Homo_sapiens.GRCh37.75.gtf.gz
```

Download the gencode gene models in GTF format.  These are the prefered gene models for use with
the STAR aligner.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/hg19/wget_gencode_gtf}
wget ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_19/gencode.v19.annotation.gtf.gz
gunzip gencode.v19.annotation.gtf.gz
```

Download the genome from ensembl.  Also run `samtools faidx` to create an index of the fasta
as this is required by many samtools related commands.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/hg19/wget_genome}
wget ftp://ftp.ensembl.org/pub/release-75/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa.gz
gunzip Homo_sapiens.GRCh37.75.dna.primary_assembly.fa.gz

samtools faidx Homo_sapiens.GRCh37.75.dna.primary_assembly.fa
```

Download the genome from ucsc, used by some tools.  Concatenate the regular chromosomes to create a
hg19.fa genome fasta.  Also run `samtools faidx`.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/hg19/wget_ucsc_genome}
rsync -avzP rsync://hgdownload.cse.ucsc.edu/goldenPath/hg19/bigZips/chromFa.tar.gz .
tar -zxvf chromFa.tar.gz

cat chr?.fa chr??.fa > hg19.fa

samtools faidx chr14.fa
```

### Index the reference genome for specific aligners

Build bowtie indices of the ensembl genome and ucsc genome.  Here the second argument is the prefix
of the fasta files, which will be how we refer to these indices in the future.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/hg19/bowtie_build_genome}
bowtie-build $ENSEMBL_GENOME_FILENAME $ENSEMBL_GENOME_PREFIX
bowtie2-build $ENSEMBL_GENOME_FILENAME $ENSEMBL_GENOME_PREFIX
bowtie-build $UCSC_GENOME_FILENAME $UCSC_GENOME_PREFIX
bowtie2-build $UCSC_GENOME_FILENAME $UCSC_GENOME_PREFIX
```

Build the gmap index for the ensembl genome.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/hg19/gmap_build_genome}
mkdir $GMAP_INDEX_DIR
gmap_build -D $GMAP_INDEX_DIR -d $GMAP_REF_ID $ENSEMBL_GENOME_FILENAME
```

Build the bwa index for the ensembl genome.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/genome/hg19/bwa_build_ens_genome}
bwa index $ENSEMBL_GENOME_FILENAME
```


