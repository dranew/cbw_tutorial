gpp -U "<#" ">" "\B" "|" ">" "<" ">" "#" "" run.md \
	| pandoc -t html \
	> run.html

gpp -U "<#" ">" "\B" "|" ">" "<" ">" "#" "" install.md \
	| pandoc -t html \
	> install.html

cat exploration.md | pandoc -t html -o exploration.html
