# Run the tophat-fusion gene fusion prediction tool

<#include "env.md">

## Execution

Running Tophat-fusion is a two step process.  First we run tophat2 to do spliced alignments, adding
the `--fusion-search` option to ensure we are looking for fusion splice alignments.  Remember to use
the prefix of the reference genome, rather than the fasta filename.

Run the tophat2 step specifying a sample specific output directory.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/tophatfusion/tophat2}
mkdir -p $TUTORIAL_HOME/analysis/tophatfusion/$SAMPLE_ID/

tophat2 -p 4 \
    --mate-inner-dist 100 \
    --mate-std-dev 30 \
    --fusion-search \
    -G $TOPHAT_GTF_FILENAME \
    -o $TUTORIAL_HOME/analysis/tophatfusion/$SAMPLE_ID/ \
    $UCSC_GENOME_PREFIX \
    $SAMPLE_FASTQ_1 $SAMPLE_FASTQ_2
```

The second step requires us to soft link some of the reference data into a directory with a specific structure.  We then run the tophat-fusion post-processing step.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/tophatfusion/tophatfusionpost}
mkdir -p $TUTORIAL_HOME/analysis/tophatfusion/$SAMPLE_ID/tophat_$SAMPLE_ID
cd $TUTORIAL_HOME/analysis/tophatfusion/$SAMPLE_ID/tophat_$SAMPLE_ID

ln -s $TUTORIAL_HOME/refdata/tophatfusion/blast
ln -s $TUTORIAL_HOME/refdata/tophatfusion/refGene.txt
ln -s $TUTORIAL_HOME/refdata/tophatfusion/ensGene.txt

tophat-fusion-post \
    --num-fusion-reads 1 \
    --num-fusion-pairs 2 \
    --num-fusion-both 5 \
    $UCSC_GENOME_PREFIX
```

