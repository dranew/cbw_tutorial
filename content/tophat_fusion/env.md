## Environment setup

Location of tophat-fusion specific gene models, ensembl but with chr prefix.

```{.bash}
TOPHAT_GTF_FILENAME=$TUTORIAL_HOME/refdata/tophatfusion/Homo_sapiens.GRCh37.75.gtf
```

