# Installation of the tophat-fusion gene fusion prediction tool

<#include "env.md">

## Installation

Install `tophat` and `tophat-fusion` using conda:

```{.bash}
conda install tophat
```

## Reference Genome Preparation

Install the reference gene annotations provided by tophat-fusion in the tophat-fusion specific
reference data directory.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/tophatfusion/downloadgenemodels}
mkdir -p $TUTORIAL_HOME/refdata/tophatfusion/
cd $TUTORIAL_HOME/refdata/tophatfusion/
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/database/refGene.txt.gz
gunzip refGene.txt.gz
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/database/ensGene.txt.gz
gunzip ensGene.txt.gz
```

We require the gene models in GTF format and we will use the GTF provided by ensembl.  However, we first need to add the 'chr' prefix to the chromosome names so we match the ucsc genome.  Use the sed command to create a modified version of the ensembl gene models with the chr prefix for each chromosome.  This can be done by just adding `chr` to the beginning of each line in the GTF
file.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/tophatfusion/ensemblgenemodels}
sed 's/^\([^#]\)/chr'$TUTORIAL_CHROMOSOME'/' $ENSEMBL_GTF_FILENAME | sed 's/^chrMT/chrM/' > $TOPHAT_GTF_FILENAME
```

Tophat requires additional blast databases, download and install these.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/tophatfusion/downloadblastdbs}
mkdir -p $TUTORIAL_HOME/refdata/tophatfusion/blast
cd $TUTORIAL_HOME/refdata/tophatfusion/blast
wget ftp://ftp.ncbi.nlm.nih.gov/blast/db/human_genomic.*.tar.gz
wget ftp://ftp.ncbi.nlm.nih.gov/blast/db/nt.*.tar.gz
gunzip *.gz
```


