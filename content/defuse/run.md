# Run the deFuse gene fusion prediction tool

<#include "env.md">

## Execution

Running deFuse involves invoking a single script using perl.  The output is to
a directory which will contain a number of temporary and results files.  Create
an output directory and run the defuse script specifying the paired end reads
and the configuration filename.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/defuse/$SAMPLE_ID/run}
mkdir -p $TUTORIAL_HOME/analysis/defuse/$SAMPLE_ID/

defuse_run.pl \
    -c $DEFUSE_CONFIG \
    -d $DEFUSE_REF_DATA \
    -1 $SAMPLE_FASTQ_1 \
    -2 $SAMPLE_FASTQ_2 \
    -o $TUTORIAL_HOME/analysis/defuse/$SAMPLE_ID/
```

The results are output as a table in TSV format at `$TUTORIAL_HOME/analysis/defuse/$SAMPLE_ID/results.filtered.tsv`.  They can be viewed in R or MS Excel.  To view the read evidence for a specific fuion, use the `defuse_get_reads.pl` script.

```
defuse_get_reads.pl \
    -c $DEFUSE_CONFIG \
    -d $DEFUSE_REF_DATA \
    -o $TUTORIAL_HOME/analysis/defuse/$SAMPLE_ID/ \
    -i 20
```
