# Installation of the deFuse gene fusion prediction tool

<#include "env.md">

## Installation

### Install deFuse

Install in ChimeraScan using `conda`.

```{.bash}
conda install defuse
```

### Install the required reference data files

The reference data files can be downloaded and index automatically using
the `defuse_create_ref.pl` script.

```{.bash sentinal=$TUTORIAL_HOME/sentinal/defuse/createrefdata}
defuse_create_ref.pl -c $DEFUSE_CONFIG -d $DEFUSE_REF_DATA
```

