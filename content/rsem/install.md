# Installation of the RNA-Seq by Expectation-Maximization (RSEM) tool

<#include "env.md">

## Installation

Install `rsem` using `conda`.

```{.bash}
conda install rsem
```

