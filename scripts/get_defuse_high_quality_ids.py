import argparse
import pandas as pd


parser = argparse.ArgumentParser()
parser.add_argument('defuse', help='Defuse Predictions')
parser.add_argument('ids', help='Filtered IDs')
args = parser.parse_args()


# Read in the results as a dataframe
results = pd.read_csv(args.defuse, sep='\t')

# Filter for exonic with high probability and no read throughs
locations = ['coding', 'utr5p', 'utr3p']
data = results[
    (results['read_through'] == 'N') &
    (results['altsplice'] == 'N') &
    (results['probability'] >= 0.8) &
    (results['gene_location1'].isin(locations)) &
    (results['gene_location2'].isin(locations))
]

# Create a list of all gene name pairs
gene_names = data[['gene_name1', 'gene_name2']]
gene_names_swapped = gene_names.copy()
gene_names_swapped.columns = ['gene_name2', 'gene_name1']
gene_names = pd.concat([gene_names, gene_names_swapped], ignore_index=True).drop_duplicates()

# Create a new filtered list of fusions that includes 
# suboptimal predictions of higher quality fused gene pairs
data = results.merge(gene_names)

# Write the cluster ids
data[['cluster_id']].to_csv(args.ids, sep='\t', index=False, header=False)


