import gzip
import pandas as pd


def _get_opener(filename):

    if filename.endswith('.gz'):
        opener = gzip.open
    else:
        opener = open
    return opener


def read_gene_regions(gene_models_filename, chromosome_subset=None, gene_name_subset=None):
    
    gene_regions = list()
    
    with _get_opener(gene_models_filename)(gene_models_filename, 'r') as gtf:
        
        for line in gtf:
            
            if line.startswith('#'):
                continue
                
            fields = line.rstrip().split('\t')
            
            if fields[2] != 'exon':
                continue
                
            keyvals = fields[-1].replace('; ', ';').rstrip(';').split(';')
            keyvals = [a.split(' ', 1) for a in keyvals]
            keyvals = [(a, b.strip('"').rstrip('"')) for a, b in keyvals]
            keyvals = dict(keyvals)
            
            keyvals['chromosome'] = fields[0]
            keyvals['start'] = int(fields[3])
            keyvals['end'] = int(fields[4])
            keyvals['strand'] = fields[6]
            
            if chromosome_subset is not None and keyvals['chromosome'] not in chromosome_subset:
                continue

            if gene_name_subset is not None and keyvals['gene_name'] not in gene_name_subset:
                continue

            gene_regions.append(keyvals)
            
    gene_regions = pd.DataFrame(gene_regions)

    gene_regions['exon_number'] = gene_regions['exon_number'].astype(int)
    
    return gene_regions


def read_sequences(fasta):

    id = None
    sequences = []

    for line in fasta:

        line = line.rstrip()

        if len(line) == 0:
            continue

        if line[0] == '>':
            if id is not None:
                yield (id, ''.join(sequences))
            id = line[1:]
            sequences = []
        else:
            sequences.append(line)

    if id is not None:
        yield (id, ''.join(sequences))


def read_genome(genome_filename, chromosome_subset=None):

    genome = dict()

    with _get_opener(genome_filename)(genome_filename, 'r') as genome_file:

        for chromosome, sequence in read_sequences(genome_file):

            chromosome = chromosome.split()[0]

            if chromosome_subset is None or chromosome in chromosome_subset:
                genome[chromosome] = sequence

    return genome
