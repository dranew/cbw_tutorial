import os
import sys
import re
import subprocess
import argparse

import utils



def read_meta(line):
    """ Read metadata of the form {.bash key=value key2=value2}
    """
    meta = dict()

    match = re.match(r'{([^}]*)}', line)

    if match is not None:
        meta_text = match.groups()[0]

        for entry in meta_text.split():
            keyval = entry.split('=')

            if len(keyval) == 2:
                meta[keyval[0]] = keyval[1]
            else:
                meta[keyval[0]] = True

    return meta



def join_split_lines(lines):
    """ Join lines split by trailing backslash
    """
    joined = list()

    current_line = ''

    for line in lines:

        line = line.rstrip()

        if line == '':
            continue

        if line.rstrip().endswith('\\'):
            current_line += line.rstrip().rstrip('\\')

        else:
            joined.append(current_line + line)
            current_line = ''

    if current_line != '':
        raise Exception('trailing backslash')

    joined = filter(lambda a: len(a.split()) > 0, joined)

    return joined



def get_code_blocks(markdown_filename):
    """ Get list of commands from markdown code blocks
    """

    in_block = False
    block_lines = list()

    with open(markdown_filename, 'r') as md:

        for line in md:
            if line.startswith('```'):
                if not in_block:
                    block_meta = read_meta(line[3:])
                    in_block = True

                else:
                    if block_meta.get('.bash', False) == True:
                        yield join_split_lines(block_lines), block_meta
                    in_block = False
                    block_lines = list()

            else:
                if in_block:
                    block_lines.append(line)


def run(bash_in, markdown_filename):

    bash_in.write('echo "markdown:" ' + markdown_filename + '\n')

    for idx, (block, meta) in enumerate(get_code_blocks(markdown_filename)):

        bash_in.write('echo "block:" ' + str(idx) + '\n')

        # Conditional execution if sentinal requested
        if 'sentinal' in meta:
            bash_in.write('if [ ! -e {0} ]; then\n'.format(meta['sentinal']))

        # Echo the code block to stdout
        for line in block:
            bash_in.write('echo "> ' + line.replace('$', '\$') + '"\n')
            
        # Execute the code blocks
        bash_in.write('\n'.join(block) + '\n')

        # Check for failure
        bash_in.write('if [ $? -ne 0 ]; then\n')
        bash_in.write('echo block {0} in {1} failed;\n'.format(idx, markdown_filename))
        bash_in.write('exit 1;\n')
        bash_in.write('fi\n')

        # Create sentinal directory if sentinal requested
        if 'sentinal' in meta:
            bash_in.write('mkdir -p {0}\n'.format(meta['sentinal']))
            bash_in.write('else\n')
            bash_in.write('echo sentinal {0} exists\n'.format(meta['sentinal']))
            bash_in.write('fi\n')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('documents', nargs='+', help='Ordered list of markdown documents')
    args = parser.parse_args()

    bash_proc = subprocess.Popen('bash', stdin=subprocess.PIPE)

    try:
        for markdown_filename in args.documents:
            run(bash_proc.stdin, markdown_filename)
    except:
        bash_proc.kill()
        raise

    bash_proc.stdin.close()
    bash_proc.wait()


