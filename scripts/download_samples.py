
import os
import subprocess
import pandas as pd

import info
import utils

sample_data_directory = os.path.join(info.data_directory, 'samples')

samples_filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'samples.tsv')

samples_table = pd.read_csv(samples_filename, sep='\t')

for idx, sample_row in samples_table.iterrows():

    sample_directory = os.path.join(sample_data_directory, sample_row['sample'])
    sample_filename = os.path.join(sample_directory, sample_row['filename'])

    if os.path.exists(sample_filename):
        continue

    with utils.CurrentDirectory(sample_directory):
        subprocess.check_call(['wget', sample_row['url']])


