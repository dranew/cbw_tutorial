
import glob
import shutil
import os
import sys
import subprocess
import tarfile

import utils
import info


Sentinal = utils.Sentinal(os.path.join(info.tools_directory, 'sentinal_'))


with Sentinal('install_sratoolkit') as sentinal:

    if sentinal.unfinished:

        utils.makedirs(info.packages_directory)
        utils.makedirs(info.bin_directory)

        with utils.CurrentDirectory(info.packages_directory):

            for filename in glob.glob('sratoolkit*'):
                if os.path.isdir(filename):
                    utils.rmtree(filename)
                else:
                    utils.remove(filename)

            if sys.platform == 'darwin':
                tgz = 'sratoolkit.current-mac64.tar.gz'
            else:
                tgz = 'sratoolkit.current-centos_linux64.tar.gz'

            subprocess.check_call('wget http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/current/{0}'.format(tgz), shell=True)
            subprocess.check_call('tar -xzvf {0}'.format(tgz), shell=True)

            sratoolkit_filenames = glob.glob('sratoolkit*')
            sratoolkit_filenames.remove(tgz)
            build = sratoolkit_filenames[0]

            subprocess.check_call('cp {0}/bin/fastq-dump* {1}'.format(build, info.bin_directory), shell=True)

