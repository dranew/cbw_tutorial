import argparse
import subprocess

import gparsers

parser = argparse.ArgumentParser()
parser.add_argument('bam', help='Bam filename')
parser.add_argument('gtf', help='Genes in GTF format')
parser.add_argument('genenames', help='Gene names filename')
parser.add_argument('out', help='Output bam prefix')
parser.add_argument('--bam_has_chr_prefix', action='store_true')
parser.add_argument('--extend_length', type=int, default=2000)
args = parser.parse_args()

gene_names = set(open(args.genenames).read().split())

gene_regions = gparsers.read_gene_regions(args.gtf, gene_name_subset=gene_names)

agg_f = {
    'chromosome':min,
    'start':min,
    'end':max,
}

gene_regions = gene_regions.groupby('gene_name').agg(agg_f).reset_index()

for idx, row in gene_regions.iterrows():

    chromosome = row['chromosome']

    if chromosome.startswith('chr'):
        chromosome = chromosome[3:]

    if args.bam_has_chr_prefix:
        chromosome = 'chr' + chromosome

    start = row['start'] - args.extend_length
    end = row['end'] + args.extend_length

    region = '{0}:{1}-{2}'.format(chromosome, start, end)

    output_bam_filename = args.out + row['gene_name'] + '.bam'

    subprocess.check_call(['samtools', 'view', '-b', args.bam, region, '-o', output_bam_filename])

    subprocess.check_call(['samtools', 'index', output_bam_filename])

